package com.HashGenerator.controller;

import com.HashGenerator.service.UniqueHashGeneratorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class MyController {

    private final UniqueHashGeneratorService uniqueHashGeneratorService;

    @GetMapping("/{id}")
    @ResponseBody
    public String welcome(final @PathVariable Integer id) {
        return this.uniqueHashGeneratorService.createHash(id);
    }

}
