package com.HashGenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HashGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(HashGeneratorApplication.class, args);
	}

}
