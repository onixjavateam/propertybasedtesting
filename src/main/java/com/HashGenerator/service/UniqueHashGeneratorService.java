package com.HashGenerator.service;

import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Date;

@Service
public final class UniqueHashGeneratorService {

    @SneakyThrows
    public String createHash(final int additionalId) {
        final String id = new Date().getTime() + "|" + additionalId;
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");

        return DatatypeConverter
                .printHexBinary(messageDigest.digest(id.getBytes(StandardCharsets.UTF_8)))
                .toLowerCase();
    }

}