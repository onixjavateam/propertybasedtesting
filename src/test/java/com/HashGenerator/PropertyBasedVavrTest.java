package com.HashGenerator;

import com.HashGenerator.service.UniqueHashGeneratorService;
import io.vavr.CheckedFunction1;
import io.vavr.test.Arbitrary;
import io.vavr.test.CheckResult;
import io.vavr.test.Property;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class PropertyBasedVavrTest {

    @Autowired
    private UniqueHashGeneratorService uniqueHashGeneratorService;

    @Test
    public void PropertyBased_Test_isUniqHash() {
        Arbitrary<Integer> multiplesOf2 = Arbitrary
                .integer();

        CheckedFunction1<Integer, Boolean> mustEquals = i -> !uniqueHashGeneratorService.createHash(i).equals(
                uniqueHashGeneratorService.createHash(i + 1)
        );

        CheckResult result = Property
                .def("Hash mast be uniq")
                .forAll(multiplesOf2)
                .suchThat(mustEquals)
                .check(10_000, 10000);
        result.assertIsSatisfied();
    }

}
