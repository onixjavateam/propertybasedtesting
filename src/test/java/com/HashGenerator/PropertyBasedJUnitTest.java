package com.HashGenerator;

import com.HashGenerator.service.UniqueHashGeneratorService;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertNotEquals;


@RunWith(JUnitQuickcheck.class)
@SpringBootTest
public class PropertyBasedJUnitTest {

    @Autowired
    UniqueHashGeneratorService uniqueHashGeneratorService;

    @Test
    @Property
    public void PropertyBased_Test_isUniqHash(@InRange(minInt = 1, maxInt = 10000) int id) {
        assertNotEquals(
                this.uniqueHashGeneratorService.createHash(id),
                this.uniqueHashGeneratorService.createHash(id + 1)
        );
    }
}
